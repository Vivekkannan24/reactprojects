import reactImage from "../../assets/react-core-concepts.png"
import './Header.css';

export default function Header(){
    return(
      <header>
          <img src={reactImage} alt="Stylized atom" />
          <h1>React Essentials</h1>
          <p>
            {dynamics[shuffle(2)]} React concepts you will need for almost any app you are
            going to build!
           </p>
        </header>
    )
  }
  
  const dynamics=['fundamental','core','concept'];
  
  function shuffle(max){
    return Math.floor(Math.random()*(1+max));
    
  }