
import componentsImg from './assets/components.png';
import propsImg from './assets/config.png';
import jsxImg from './assets/jsx-ui.png';
import stateImg from './assets/state-mgmt.png';
import { CORE_CONCEPTS } from "./app";  
import Header from "./components/Header/Header" 
import CoreConcept from './components/CoreConcepts';
import TapButton from './components/TapButton';
import { useState } from 'react';
import { EXAMPLES } from './app';

function App() {
  const[selectedTopic,setSelectedTopic]= useState('components');

 function handleClick(selectedButton){
      setSelectedTopic(selectedButton)
  }
  return (
    <div>
      <Header/>
      <main>
       <section id="core-concepts">
        <h2>core concept</h2>
        <ul>
        {CORE_CONCEPTS.map((conceptItem) =>(
          <CoreConcept{...conceptItem}/>
        ))}
          
        </ul>
       </section> 
       <section id="examples">
          <h2>Examples</h2>
          <menu>
              <TapButton isSelected={selectedTopic ==='components'} onSelect ={() => handleClick('components')}>Components</TapButton>           
              <TapButton isSelected={selectedTopic ==='props'} onSelect ={() => handleClick('props')}>Props</TapButton>           
              <TapButton isSelected={selectedTopic ==='jsx'} onSelect ={() => handleClick('jsx')}>Jsx</TapButton>           
              <TapButton isSelected={selectedTopic ==='state'} onSelect ={() => handleClick('state')}>State</TapButton>           
          </menu>
          <div id="tab-content">
            <h3>
              {EXAMPLES[selectedTopic].title}
            </h3>
            <p>
              {EXAMPLES[selectedTopic].description}
            </p>
            <pre>
              <code>
                {EXAMPLES[selectedTopic].code}
              </code>
            </pre>
          </div>
       </section>
      </main>
    </div>
  );
}
export default App;
