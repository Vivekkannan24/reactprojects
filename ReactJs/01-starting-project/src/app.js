
import componentImage from './assets/components.png';
import configImage from './assets/config.png';
import configImage2 from './assets/jsx-ui.png';
import state from './assets/state-mgmt.png';

export const CORE_CONCEPT =[{
    image:{componentImage},
    title:"components",
     description:"the core UI building blocks"
     
},
   
{
    image:{configImage},
    title:"system",
    description:"settings you can manipulate"
   
},

{
    image:{configImage2},
    title:"UI components",
    description:"with which user you can interact" 
 
},

{
    image:{state}, 
    title:"DataBase",
    description:"where you can save your data's"
    
}
] 

