import reactImage from './assets/react-core-concepts.png';
import {CORE_CONCEPT} from './app.js';

const description =['fundamental','core','concept'];

function dynamicOutput(num){
  return Math.floor(Math.random()*(num+1));
}

function Header(){
  const desc = description[dynamicOutput(2)]
  return(
    <header>

        <img src={reactImage} alt="Stylized atom" />
        <h1>React Essentials</h1>
        <p>
        {desc} React concepts you will need for almost any app you are
          going to build!
        </p>
      </header>
  )
}

function CoreConcept(props){
 return(
  <li>
      <img src={props.image} alt="title" />
      <h3>{props.title}</h3>
      <p>{props.description}</p>
  </li>
 );

}

function App() {
  return (
    <div>
      <Header/>
      <main>
        <section id="core-concepts">
          <h2>Core Concepts</h2>
          <ul>
              <CoreConcept
                image={CORE_CONCEPT[0]}
                title={CORE_CONCEPT[0]}
                description={CORE_CONCEPT[0]}
                
              />
              <CoreConcept
                 {...CORE_CONCEPT[1]} /> 
              <CoreConcept
              {...CORE_CONCEPT[2]}/>
              <CoreConcept
                 {...CORE_CONCEPT[3]}/>
          </ul>
        </section>
        <h2>Time to get started!</h2>
      </main>
    </div>
  );
}

export default App;
